(define
    (s (lambda (x) (cons x nil)))
    (p car)
    (peano (lambda (n) (cond ((eq n 0) nil) (T (s (peano (sum n -1)))))))
    (add (lambda (x y) (cond ((equal x nil) y) (T (add (p x) (s y))))))
    (mul (lambda (x y) (cond ((equal y (s nil)) x) (T (add x (mul x (p y)))))))
    (fac (lambda (n) (cond ((equal n nil) (s nil)) (T (mul n (fac (p n)))))))
    (count (lambda (x) (cond ((equal x nil) 0) (T (sum 1 (count (car x))))))))
