use criterion::{black_box, criterion_group, criterion_main, Criterion};
use lispi::{
    interpreter::eval,
    parser::parse,
    types::{NullableList, NIL},
};

fn fac_10(c: &mut Criterion) {
    let fac = "(define (fac (lambda (n) (cond ((equal n 0) 1) (T (prdct n (fac (sum n -1))))))))";
    let fac_def = parse(fac).unwrap();

    // must create this nil variable because LSP claims that:
    // temporary value dropped while borrowed
    // creates a temporary value which is freed while still in use
    let nil: NullableList = NIL.into();
    let a_list = eval(fac_def, &nil).unwrap().1;
    let fac_test = parse("(fac 10)").unwrap();

    c.bench_function("factorial of 10", |b| {
        b.iter(|| eval(black_box(fac_test.clone()), black_box(&a_list)))
    });
}

fn peano_fac_4(c: &mut Criterion) {
    let defs = parse(include_str!("peano.lisp")).unwrap();

    // must create this nil variable because LSP claims that:
    // temporary value dropped while borrowed
    // creates a temporary value which is freed while still in use
    let nil: NullableList = NIL.into();
    let a_list = eval(defs, &nil).unwrap().1;
    let test_expr = parse("(count (fac (peano 4)))").unwrap();

    c.bench_function("peano factorial of 4", |b| {
        b.iter(|| eval(black_box(test_expr.clone()), black_box(&a_list)))
    });
}
criterion_group!(benches, fac_10, peano_fac_4);
criterion_main!(benches);
