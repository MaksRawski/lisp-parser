use criterion::{black_box, criterion_group, criterion_main, Criterion};
use enum_iterator::all;
use lispi::{
    repl::MyHighlighter,
    types::{BuiltinFunc, SpecialForm},
};

fn bracket_highlighting(c: &mut Criterion) {
    let test_str = format!("{}{}", "(".repeat(100), ")".repeat(99));
    c.bench_function("bracket highlighting", |b| {
        b.iter(|| MyHighlighter::highlight_brackets(black_box(&test_str), black_box(0)))
    });
}

fn keyword_highlighting(c: &mut Criterion) {
    let special_forms = all::<SpecialForm>()
        .map(|v| format!("({:?})", v))
        .collect::<String>();
    let builtin_funcs = all::<BuiltinFunc>()
        .map(|v| format!("({:?})", v))
        .collect::<String>();

    let test_str = format!("{} {}", special_forms, builtin_funcs);
    c.bench_function("keyword highlighting", |b| {
        b.iter(|| MyHighlighter::highlight_syntax(black_box(&test_str)))
    });
}

criterion_group!(benches, bracket_highlighting, keyword_highlighting);
criterion_main!(benches);
